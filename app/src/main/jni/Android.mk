TOP_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)
LOCAL_MODULE := fftw3
LOCAL_SRC_FILES := ./fftw3/lib/libfftw3.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/fftw3/include
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/process
LOCAL_MODULE   := process
LOCAL_SRC_FILES := process.c
LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_CFLAGS := -g
LOCAL_LDLIBS := -llog -lm
LOCAL_STATIC_LIBRARIES := fftw3
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)
LOCAL_PATH := $(TOP_PATH)/dct
LOCAL_MODULE   := dct
LOCAL_SRC_FILES := dct.c
LOCAL_C_INCLUDES := $(LOCAL_PATH)
LOCAL_CFLAGS := -g
LOCAL_LDLIBS := -llog -lm
LOCAL_STATIC_LIBRARIES := fftw3
include $(BUILD_SHARED_LIBRARY)

#include $(CLEAR_VARS)
#include /Users/davidjun/NVPACK/OpenCV-2.4.3.2-android-sdk-tadp/sdk/native/jni/OpenCV.mk
#LOCAL_MODULE    := mixed_sample
#LOCAL_SRC_FILES := jni_part.cpp
#LOCAL_LDLIBS +=  -llog -ldl
#include $(BUILD_SHARED_LIBRARY)
