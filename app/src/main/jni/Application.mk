APP_PLATFORM := android-16
APP_ABI := armeabi-v7a
APP_STL := gnustl_static
APP_CPPFLAGS := -frtti -fexceptions
APP_OPTIM:=debug
APP_MODULES := dct process
