#include <jni.h>
#include <math.h>
#include <android/log.h>

#include "../fftw3/include/fftw3.h"
#include "../window.h"

JNIEXPORT void JNICALL Java_com_longle1_spectrogram_MFCC_dct
	(JNIEnv *env, jclass obj, jdoubleArray jinbuf, jdoubleArray joutbuf, jint N)
{
	int i;
	double* inBuf = (double*)(*env)->GetDoubleArrayElements(env,jinbuf,0);
	double* outBuf = (double*)(*env)->GetDoubleArrayElements(env,joutbuf,0);
	double *in = (double*) fftw_malloc(sizeof(double) * N);
	double *out = (double*) fftw_malloc(sizeof(double) * N);
	fftw_plan p;

	for(i=0; i<N; i++) {
		in[i] = inBuf[i];
	}

	p = fftw_plan_r2r_1d(N, in, out, FFTW_REDFT10, FFTW_ESTIMATE);
	fftw_execute(p);

	for(i=0; i<N; i++) {
		outBuf[i] = out[i]/sqrt(N/2)/2; // extra scaling factor to match with MatLab DCT definition
	}

	(*env)->ReleaseDoubleArrayElements(env, jinbuf, inBuf, 0);
	(*env)->ReleaseDoubleArrayElements(env, joutbuf, outBuf, 0);

	fftw_destroy_plan(p);
	fftw_free(in);
	fftw_free(out);
}

