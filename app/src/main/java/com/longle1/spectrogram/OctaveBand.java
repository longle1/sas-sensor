package com.longle1.spectrogram;

import java.util.ArrayList;
public class OctaveBand {
	public ArrayList<double[]> feat;
	private int nAverageFrame ;
	private int counter =0;
	private int band= 8;// octaveband 
	private int[] bandlens ={1,2,4,8,16,32,64,128};
	private double[] sumEnergy;

	public OctaveBand(int nAverageFrame) {
		this.nAverageFrame= nAverageFrame;
		this.feat=  new ArrayList<>();
		this.sumEnergy = new double[this.band];
	}
	
	public void update(double[] frame){
		int bandStartInd;
		int bandStopInd=1;
		
		
		if (frame.length <256)
			return; // no update if frame.length less than 256
		
		for (int k = 0; k < sumEnergy.length; k++) {		
			bandStartInd = bandStopInd;
			bandStopInd = bandStartInd+this.bandlens[k];
			
			for (int i = bandStartInd; i < bandStopInd; i++) {
				sumEnergy[k]+=frame[i]*frame[i];		
			}	
		}
		this.counter++;
		if (counter == this.nAverageFrame){
			for (int i = 0; i < sumEnergy.length; i++) {
				sumEnergy[i]=sumEnergy[i]/this.nAverageFrame;
			}
			this.feat.add(sumEnergy);
			
			// reset SumEnergy
			this.sumEnergy =  new double[this.band];
			// reset counter
			this.counter=0;
		}
		
	}
	
	public void clearState(){
        this.feat = new ArrayList<>();
        this.counter =0;
        this.sumEnergy =  new double[this.band];
        
    }
	
	public ArrayList<double[]> getFeat(){
        return feat;
    }

}
