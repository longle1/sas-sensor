package com.longle1.spectrogram;

public class ComparePair implements Comparable<ComparePair> {
    public final int index;
    public final double value;

    public ComparePair(int index, double value) {
        this.index = index;
        this.value = value;
    }

    @Override
    public int compareTo(ComparePair other) {
        //multiplied to -1 as the author need descending sort order
        return -1 * Double.valueOf(this.value).compareTo(other.value);
    }
}
