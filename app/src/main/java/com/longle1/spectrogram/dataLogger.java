package com.longle1.spectrogram;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Long on 3/31/2016.
 */
public class DataLogger {
    File mDatDir;
    DataLogger(File datDir){
        mDatDir = datDir;
    }

    public void log(String fn,JSONObject jsonObj){
        if (jsonObj != null) {
            try {
                File file = new File(mDatDir, fn);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
                outputStreamWriter.write(jsonObj.toString());
                outputStreamWriter.flush();
                outputStreamWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("feature", "logged");
        }
    }

    public void log(String fn,byte[] wavebb){
        if (wavebb != null) {
            try {
                File file = new File(mDatDir, fn + ".wav");
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
                bufferedOutputStream.write(wavebb);
                bufferedOutputStream.flush();
                bufferedOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.i("audio", "logged");
        }
    }
}
