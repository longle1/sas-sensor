/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

import com.longle1.RidgeTracker;

import java.io.Serializable;
import java.util.ArrayList;
public class GUIFrame implements Serializable{
	private static final long serialVersionUID = -3806069573164225068L;
	
	double[] spec;
	public ArrayList<Integer> FI = new ArrayList<Integer>();
	public ArrayList<Integer> TI = new ArrayList<Integer>();
	//public ArrayList<Integer> groupId = new ArrayList<Integer>();
	//public int backtracklen;
	//public int currTime;
	//public int maxHoldTime;
	
	public GUIFrame(double []frame){
		spec = new double[frame.length];
		
		for (int i = 0; i < frame.length; i++){
			spec[i] = 1-Math.exp(-Math.pow(frame[i],2));
			//spec[i] = spec[i] < 0.0?0.0:(spec[i] > 1.0?1.0:spec[i]);
			
			// ridge link back
			//spec[i] = (double)(ridgetracker.stftdetectridgeback.get(1)[i]+1)/10.0;
		}
	}
	
	public void update(RidgeTracker ridgetracker){
		//backtracklen = ridgetracker.backtracklen;
		//maxHoldTime = ridgetracker.maxHoldTime;
		//currTime = ridgetracker.currTime;
		// copy ridges in selected group for GUI
		for (int k = 0; k < ridgetracker.FI.size(); k++){
			//groupId.add(ridgetracker.groupId.get(ridgetracker.selGId[k]));
			FI.add(ridgetracker.FI.get(k));
			TI.add(ridgetracker.TI.get(k));
		}
	}
}
