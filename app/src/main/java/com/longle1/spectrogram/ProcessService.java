/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.longle1.RidgeTracker;
import com.longle1.spectrogram.Spectrogram.ResponseReceiver;
import com.loopj.android.http.RequestParams;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

public class ProcessService extends Service {
	private Context			mContext;
	private int				fs;
	private int 			channelConfiguration;
	private int 			audioEncoding;
	
	private int 			blockSize = 512;
	private int				freqSize = blockSize/2;
	private int				increSize = blockSize/2;

	private File			datDir = null;
	private String 			targetURI = null;
	private String			db = null;
	//private String			dev = null;
	private String			pwd = null;
	private String 			android_id = null; 
	private int				thresh;
	private int 			datSrc;
	//private boolean 		isDebug;
	private boolean 		isSendFeat;
	//private int				featType;
	private boolean 		isSendAudio;
	private boolean			isEvent;
	private RequestParams 	rp;
	private RequestParams 	rpGrid;
	
	private Looper			mAsyncHttpLooper;
	private Looper 			mServiceLooper;

	private BatteryLevelMonitor mBatteryLevelMonitor;
	//private CommunicationManager mCommunicationManager;
	private LocationData locationListener; // location variable declaration, amended by Duc, Aug,28 
	private AudioRecord audioRecord;
	private BroadcastReceiver networkStateReceiver;
	private Trepner trepn;

	@Override
	public void onCreate() {
		// Start up the thread running the service.  Note that we create a
	    // separate thread because the service normally runs in the process's
	    // main thread, which we don't want to block.  We also make it
	    // background priority so CPU-intensive work will not disrupt our UI.
	    
		HandlerThread thread = new HandlerThread("ServiceHandler",
	    		android.os.Process.THREAD_PRIORITY_BACKGROUND);
	    thread.start();
	    
	    // Get the HandlerThread's Looper and use it for our Handler
	    mServiceLooper = thread.getLooper();
	    
	    HandlerThread thread2 = new HandlerThread("AsyncHttpResponseHandler",
	    		android.os.Process.THREAD_PRIORITY_BACKGROUND);
	    thread2.start();
	    
	    mAsyncHttpLooper = thread2.getLooper();
	    
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mContext = this;
		Toast.makeText(mContext, "service starting", Toast.LENGTH_SHORT).show();
		Log.i("ProcessService", "service starting");
		
		Bundle bundle = intent.getBundleExtra("Menu.Bundle");
		targetURI = bundle.getString("serverURI");
		//dev = bundle.getString("devName");
		pwd = bundle.getString("password");
		db = bundle.getString("database");
		android_id = bundle.getString("androidID");
		thresh = bundle.getInt("thresh");
		datSrc = bundle.getInt("datSrc");
		//isDebug = bundle.getBoolean("isDebug");
		isSendFeat = bundle.getBoolean("isSendFeat");
		//featType = bundle.getInt("featType");
		isSendAudio = bundle.getBoolean("isSendAudio");
		isEvent = bundle.getBoolean("isEvent");

		// init communication manager
		//mCommunicationManager = new CommunicationManager(this);
		// init battery level monitor
		mBatteryLevelMonitor = new BatteryLevelMonitor(mContext);
		//initialize the locationListener
		locationListener = new LocationData(mContext);

		trepn = new Trepner(mContext);
		trepn.stateUpdate(0,"");

		datDir = getDatDir();
		networkStateReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.i("Network Listener", "Network Type Changed");

				new CommTask(mAsyncHttpLooper,mContext,datDir,targetURI,db,pwd).execute();
			}
		};
		IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(networkStateReceiver, filter);
		
		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		ServiceHandler serviceHandler = new ServiceHandler(mServiceLooper);
		Message msg = serviceHandler.obtainMessage();
		msg.arg1 = startId;
		serviceHandler.sendMessage(msg);
		
		// We want this service to continue running until it is explicitly
	    // stopped, so return sticky.
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
	    // We don't provide binding, so return null
	    return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		locationListener.stopLocationUpdate();
		trepn.stateUpdate(0,"");
		try{
			audioRecord.stop();
			audioRecord.release();
		}
		catch(IllegalStateException e) {
			Log.e("ProcessService", e.toString());
		}
		unregisterReceiver(networkStateReceiver);
		Toast.makeText(mContext, "service done", Toast.LENGTH_SHORT).show();
		Log.i("ProcessService", "service done");
	}

	// Handler that receives messages from the thread
	private final class ServiceHandler extends Handler {

		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			if (datSrc == 0){ //mic
				fs = 16000;
				channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
				audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
				audioRecord = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
						fs,
						channelConfiguration,
						audioEncoding,
						AudioRecord.getMinBufferSize(fs, channelConfiguration, audioEncoding));
			}else{ // bluetooth
				fs = 6800;
				channelConfiguration = AudioFormat.CHANNEL_IN_MONO;
				audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
				audioRecord = new BtAudioRecord(MediaRecorder.AudioSource.DEFAULT,
						fs,
						channelConfiguration,
						audioEncoding,
						AudioRecord.getMinBufferSize(fs, channelConfiguration, audioEncoding),
						mContext);
			}

			short[] buffer = new short[increSize];
			CircularFifoQueue<Short> winBuffer = new CircularFifoQueue<Short>(blockSize);
			// size is still 0, must initialize
			for (int k=0;k<blockSize;k++){
				winBuffer.add(new Short((short) 0));
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			trepn.stateUpdate(1,"main");
			try{
				audioRecord.startRecording();
			}
			catch(IllegalStateException e) {
				Toast mToast = Toast.makeText(mContext, "Failed to start recording", Toast.LENGTH_SHORT);
				mToast.setGravity(Gravity.TOP, 0, 0);
				TextView v = (TextView) mToast.getView().findViewById(android.R.id.message);
				v.setTextColor(Color.RED);
				mToast.show();

				GlobalState.getGlobalState().setBGstate(false);
			}

			// initialize
			RidgeTracker ridgetracker = null;
			MFCC mMFCC = null;
			CircularFifoQueue<byte[]> prevBuffer = null;
			OctaveBand octaveBand;
			int count = -1;
			if (isEvent){
				ridgetracker = new RidgeTracker(fs, blockSize, increSize, thresh);
				mMFCC = new MFCC(16);
				octaveBand = new OctaveBand(31);

				int numLag = (int)Math.ceil(0.2*fs/(increSize));
				prevBuffer = new CircularFifoQueue<byte[]>(numLag);
				for (int k = 0; k < numLag; k++){
					prevBuffer.add(new byte[blockSize]);
				}
			}else{
				octaveBand = new OctaveBand(62); // 1 second of data
				count = 0;
			}
			// the main loop
			while( GlobalState.getGlobalState().getBGstate() ) {
				// read in audio data, 50% overlapping
				audioRecord.read(buffer, 0, increSize);
				for (int k=0;k<buffer.length;k++){
					winBuffer.add(buffer[k]);
				}

				// to the frequency domain
				short[] inFrame = new short[winBuffer.size()];
				for (int k=0;k<winBuffer.size();k++){
					inFrame[k] = winBuffer.get(k).shortValue();
				}
				double[] outFrame = new double[freqSize];
				process(inFrame, outFrame, freqSize);

				// New guiFrame without overlay
				GUIFrame guiFrame = new GUIFrame(outFrame);

				// (frequency-domain) signal processing
				if (isEvent){
					ridgetracker.update(outFrame);
					if (ridgetracker.FI.size()>0) {
						mMFCC.update(outFrame);
						octaveBand.update(outFrame);
					} else {
						if (mMFCC.feat.size() > 0){
							mMFCC.clearState();
						}
						if (octaveBand.feat.size() > 0){
							octaveBand.clearState();
						}
					}
				}else{
					count = count + 1;
					octaveBand.update(outFrame);
				}

				// Raw recording
				byte[] buffer2 = new byte[buffer.length * 2];
				ByteBuffer.wrap(buffer2).order(ByteOrder.nativeOrder()).asShortBuffer().put(buffer);
				if (isEvent){
					prevBuffer.add(buffer2);
					if (ridgetracker.FI.size()>0){
						trepn.stateUpdate(2, "tracking");
						try {
							if (baos.size() == 0){
								for (int k = 0; k < prevBuffer.size(); k++){
									baos.write(prevBuffer.get(k));
								}
							}
							baos.write(buffer2);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}else if (baos.size() > 0){
						baos.reset();
					}
				} else{
					try {
						baos.write(buffer2);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				/*
				// Control wifi status
				if (isEvent){
					mCommunicationManager.wifiControl(ridgetracker.isReady);
				}
				*/

				// check for the end of a period
				boolean isEnd = false;
				if (isEvent){
					if (ridgetracker.isReady){
						isEnd =true;
					}
				} else{
					if (count >= 3750){// update period = increSize/fs*3750 second
						isEnd = true;
					}
				}

				if (isEnd){
					// transmission
					trepn.stateUpdate(3, "log/transmit");
					/*
					SimpleDateFormat fnf = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US); // Locale.US for machine readability
					fnf.setTimeZone(TimeZone.getTimeZone("UTC")); // otherwise, SimpleDateFormat uses a default system timezone
					String fn = fnf.format(new Date()); // name file using time stamp
					*/
					String fn = UUID.randomUUID().toString();

					if (isSendAudio) {
						// *** send wave files
						WaveFile wavfile = new WaveFile((byte) 16, fs, baos.size());
						byte[] wavebb = wavfile.toWaveByteBuffer(baos.toByteArray());
						new DataLogger(datDir).log(fn, wavebb);
					}

					if (isSendFeat) {
						// *** send metadata
						JSONObject jsonObj = basicJSON(fn);
						addFeatJSON(jsonObj, ridgetracker, mMFCC, octaveBand);
						new DataLogger(datDir).log(fn, jsonObj);
					}
					new CommTask(mAsyncHttpLooper,mContext,datDir,targetURI, db, pwd).execute();

					// update gui frame and cleanup
					guiFrame.update(ridgetracker);
					// reset states & recording
					if (isEvent){
						ridgetracker.clearState();
						mMFCC.clearState();
						octaveBand.clearState();
					} else{
						octaveBand.clearState();
						count = 0;
					}
					baos.reset();

					trepn.stateUpdate(1, "main");
				}

				// update screen with processed results
				if( GlobalState.getGlobalState().getGUIstate() ){
					Intent broadcastIntent = new Intent();
					broadcastIntent.setAction(ResponseReceiver.ACTION_RESP);
					broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
					broadcastIntent.putExtra("GUIFrame", guiFrame);
					sendBroadcast(broadcastIntent);
				}
			}

			// Stop the service using the startId, so that we don't stop
			// the service in the middle of handling another job
			stopSelf(msg.arg1);
		}

		private JSONObject basicJSON(String fn){
			JSONObject json = new JSONObject();
			try{
				json.put("device","Android");
				json.put("androidID", android_id);
				json.put("serviceAddr", new URL(targetURI).getHost());
				//json.put("key",pwd);
				json.put("filename", fn+".wav");
				json.put("fs", fs);
				json.put("blkSize", blockSize);
				json.put("freSize", freqSize);
				json.put("incSize", increSize);
				json.put("freSize", freqSize);
				json.put("batteryLevel", mBatteryLevelMonitor.getBatteryLevel());

				JSONObject recordDateJson = new JSONObject();
				SimpleDateFormat tsf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US); // ISO8601 uses trailing Z
				tsf.setTimeZone(TimeZone.getTimeZone("UTC"));
				String ts = tsf.format(new Date());
				recordDateJson.put("$date", ts);
				json.put("recordDate", recordDateJson);

				JSONObject locationJson = new JSONObject();
				locationJson.put("type", "Point");
				JSONArray coord = new JSONArray();
				coord.put(locationListener.getLongtitude());
				coord.put(locationListener.getLatitude());
				locationJson.put("coordinates",  coord);
				json.put("location", locationJson);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (MalformedURLException e){
				e.printStackTrace();
			}

			return json;
		}

		private void addFeatJSON(JSONObject json,RidgeTracker ridgetracker, MFCC mMFCC,OctaveBand octaveBand){
			// octaveFeat
			try{
				ArrayList<double[]> octaveFeat = octaveBand.getFeat();
				JSONArray octaveFeatJson = new JSONArray();
				for (int i = 0; i < octaveFeat.size(); i++) {
					JSONArray octaveFeatIdxJson = new JSONArray();
					for (int j = 0; j < octaveFeat.get(i).length; j++) {
						octaveFeatIdxJson.put(octaveFeat.get(i)[j]);
					}
					octaveFeatJson.put(octaveFeatIdxJson);
				}
				json.put("octaveFeat",octaveFeatJson);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (NullPointerException e){
				Log.e("addFeatJSON","null pointer");
			}

			// TF ridges
			try{
				json.put("maxDur", ridgetracker.getMaxDur());
				json.put("maxFreq", ridgetracker.getMaxFreq());
				json.put("minFreq", ridgetracker.getMinFreq());
				json.put("avgSNR", ridgetracker.getAvgSNR());
				/*
				double[] TFRidgeFeat = ridgetracker.getFeat();
				JSONArray TFRidgeFeatJson = new JSONArray();
				for (int i = 0; i < TFRidgeFeat.length; i++) {
					TFRidgeFeatJson.put(TFRidgeFeat[i]);
				}
				*/
				/*
				HashMap<Integer,ArrayList<Integer>> TFRidgeFeat = ridgetracker.getFeat();
				JSONObject TFRidgeFeatJson = new JSONObject();
				for ( Integer key : TFRidgeFeat.keySet() ) {
					ArrayList<Integer> freqIndex = TFRidgeFeat.get(key);
					JSONArray freqJson = new JSONArray();
					for ( Integer ele : freqIndex.toArray(new Integer[freqIndex.size()]) ){
						freqJson.put(ele.intValue());
					}
					TFRidgeFeatJson.put('t'+key.toString(),freqJson);
				}
				json.put("TFRidgeFeat", TFRidgeFeatJson);
				*/
				HashMap<String,ArrayList> TFRidgeFeat = ridgetracker.getFeat();
				JSONObject TFRidgeFeatJson = new JSONObject();
				for ( String key : TFRidgeFeat.keySet() ) {
					ArrayList arrayList = TFRidgeFeat.get(key);
					JSONArray arrayJson = new JSONArray();
					for ( Object ele : arrayList ){
						arrayJson.put(ele);
					}
					TFRidgeFeatJson.put(key.toString(),arrayJson);
				}
				json.put("TFRidgeFeat", TFRidgeFeatJson);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (NullPointerException e){
				Log.e("addFeatJSON","null pointer");
			}

			// MFCC
			try{
				ArrayList<double[]> MFCCFeat = mMFCC.getFeat();
				JSONArray MFCCFeatJson = new JSONArray();
				for (int i = 0; i < MFCCFeat.size(); i++) {
					JSONArray MFCCFeatIdxJson = new JSONArray();
					for (int j = 0; j < MFCCFeat.get(i).length; j++) {
						MFCCFeatIdxJson.put(MFCCFeat.get(i)[j]);
					}
					MFCCFeatJson.put(MFCCFeatIdxJson);
				}
				json.put("MFCCFeat", MFCCFeatJson);
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (NullPointerException e){
				Log.e("addFeatJSON","null pointer");
			}

			/*
			double[] TFRidgeFeat = ridgetracker.getFeat();
			JSONArray TFRidgeFeatJson = new JSONArray();
			for (int i = 0; i < TFRidgeFeat.length; i++) {
				TFRidgeFeatJson.put(TFRidgeFeat[i]);
			}
			json.put("TFRidgeFeat", TFRidgeFeatJson);

			JSONArray FIJson = new JSONArray();
			JSONArray TIJson = new JSONArray();
			//JSONArray MJson = new JSONArray();
			//JSONArray groupIdJson = new JSONArray();
			for (int i=0;i<ridgetracker.FI.size();i++){
				FIJson.put(ridgetracker.FI.get(i));
				TIJson.put(ridgetracker.TI.get(i));
				//MJson.put(ridgetracker.M.get(i));
				//groupIdJson.put(ridgetracker.groupId.get(i));
			}
			json.put("FI", FIJson);
			json.put("TI", TIJson);
			//json.put("M", MJson);
			//json.put("groupId", groupIdJson);
			*/
		}
	}

	private File getDatDir (){
		String sdcard = Environment.getExternalStorageDirectory().getPath();
		File dir = new File(sdcard, mContext.getString(mContext.getApplicationInfo().labelRes));
		if(!dir.exists()){
			dir.mkdirs();
		}
		File datDir = new File(dir,"dat");
		if(!datDir.exists()){
			datDir.mkdirs();
		}
		return datDir;
	}

	public static native void process(short[] inbuf, double[] outbuf, int N);

}
