/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

import android.app.Application;

public class GlobalState extends Application {
	private boolean	enableBG = false; // background state
	private boolean	enableGUI = false; // GUI state
	private static GlobalState instance;
	
	public void onCreate(){
		super.onCreate();
    	instance = this;
	}
	
	public static GlobalState getGlobalState() {
	    return instance;
	  }
	
	public boolean getBGstate() {
        return enableBG;
    }
	public boolean getGUIstate() {
        return enableGUI;
    }

    public void setBGstate(boolean BGstate) {
        this.enableBG = BGstate;
    }
    public void setGUIstate(boolean GUIstate) {
        this.enableGUI = GUIstate;
    }
}
