/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
/*
 * Location class for location retrieval
 * Author: Long Le, longle1@illinois.edu
 * 09-10-2014
 * 
 */
public class LocationData implements 
	GoogleApiClient.ConnectionCallbacks,
	GoogleApiClient.OnConnectionFailedListener,
	LocationListener {
        // Milliseconds per second
        private static final int MILLISECONDS_PER_SECOND = 1000;
        // Update frequency in seconds
        public static final int UPDATE_INTERVAL_IN_SECONDS = 60;
        // Update frequency in milliseconds
        private static final long UPDATE_INTERVAL =
                MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
        // The fastest update frequency, in seconds
        private static final int FASTEST_INTERVAL_IN_SECONDS = 15;
        // A fast frequency ceiling in milliseconds
        private static final long FASTEST_INTERVAL =
                MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
        
        private final Context mContext;
	    
    	private Location location;
    	private double latitude; // latitude
        private double longitude; // longitude
        
    	private LocationRequest mLocationRequest;
    	private GoogleApiClient  mLocationClient;
    	
    	public LocationData(Context context){
    		mContext = context;

    		// Create the LocationRequest object
            mLocationRequest = LocationRequest.create();
            // Use high accuracy
            mLocationRequest.setPriority(
                    LocationRequest.PRIORITY_HIGH_ACCURACY);
            // Set the update interval to 5 seconds
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            // Set the fastest update interval to 1 second
            mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
            
            int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
            if (ConnectionResult.SUCCESS == resultCode){
	            mLocationClient = new GoogleApiClient.Builder(context)
						            .addApi(LocationServices.API)
						            .addConnectionCallbacks(this)
						            .addOnConnectionFailedListener(this)
						            .build();
	            mLocationClient.connect();
            } else{
            	Log.e("Google Play Services", "Not available");
				Toast.makeText(mContext, "isGooglePlayServicesAvailable errorCode "+String.valueOf(resultCode), Toast.LENGTH_LONG).show();
            }
    	}
    	
		 /*
	     * Called by Location Services when the request to connect the
	     * client finishes successfully. At this point, you can
	     * request the current location or start periodic updates
	     */
        @Override
        public void onConnected(Bundle dataBundle) {
            // Display the connection status
        	Log.i("Google Play Services", "Connected");
        	// Request location updates for a listener
        	LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
        }
        /*
         * Called by Location Services if the connection to the
         * location client drops because of an error.
         */
        @Override
        public void onConnectionSuspended(int i) {
            // Display the connection status
        	Log.i("Google Play Services", "Disconnected. Please re-connect.");
        }
        /*
         * Called by Location Services if the attempt to
         * Location Services fails.
         */
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            /*
             * Google Play services can resolve some errors it detects.
             * If the error has a resolution, try sending an Intent to
             * start a Google Play services activity that can resolve
             * error.
             */
            if (connectionResult.hasResolution()) {
            	Log.e("Google Play Services", "Connection Failed: Has resolution.");
            } else {
            	Log.e("Google Play Services", "Connection Failed: No resolution.");
            }
        }
        
        @Override
		public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            this.location=location;
            if (location != null) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
            if( GlobalState.getGlobalState().getGUIstate() ){
	        	String msg = "onLocationChanged: "+
	        			String.format("%.5f,",latitude)+
	            		String.format("%.5f",longitude);
	            Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
            } else{
            	Log.i("onLocationChanged", Double.toString(latitude)+"," +Double.toString(longitude));
            }
        }
        
        public double getLatitude(){
			if(location != null){
	            latitude = location.getLatitude();
	        }
	         
	        // return latitude
			return latitude;
		}
		public double getLongtitude(){
			if(location != null){
	            longitude = location.getLongitude();
	        }
			return longitude;
		}
		
		public void stopLocationUpdate(){
			if (mLocationClient == null){
				return;
			}
			// If the client is connected
	        if (mLocationClient.isConnected()) {
	            /*
	             * Remove location updates for a listener.
	             * The current Activity is the listener, so
	             * the argument is "this".
	             */
	        	LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, this);
	        	/*
		         * After disconnect() is called, the client is
		         * considered "dead".
		         */
		        mLocationClient.disconnect();
	        }
	        mLocationClient = null;
		}
}
	    

