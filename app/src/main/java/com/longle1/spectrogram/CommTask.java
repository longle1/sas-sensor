package com.longle1.spectrogram;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommTask extends AsyncTask<Void, Double, Long>{
	Looper mAsyncHttpLooper;
	Context mContext;
	String mTargetURI,mDb, mPwd;
	//Boolean mIsEvent;
	File mDatDir;
	ConnectivityManager cm;
	//Trepner trepn;

	public CommTask(Looper asyncHttpLooper,Context baseContext,File datDir,String targetURI,String db, String pwd){
		mAsyncHttpLooper = asyncHttpLooper;
		mContext = baseContext;
		mTargetURI = targetURI;
		mDb = db;
		mPwd = pwd;
		//mIsEvent = isEvent;
		mDatDir = datDir;
		cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		//trepn = new Trepner(mContext);
	}

	protected void onPreExecute(){
		Log.i("CommTask","onPreExecute()");
		//trepn.stateUpdate(3, "CommTask.onPreExecute()");
	}

	@Override
	protected Long doInBackground(Void... params) {
		if (isConnected()) {
			List<File> fileList = getListFiles(mDatDir);
			//fileList = fileList.subList(0,99);
			double cnt = 0;
			for (File file : fileList) {
				try {
					if ( file.getName().endsWith(".wav") ){
						byte[] bytes = new byte[(int) file.length()];
						BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
						buf.read(bytes, 0, bytes.length);
						buf.close();

						RequestParams rp = new RequestParams();
						rp.put("dbname", mDb);
						rp.put("colname", "data");
						/*
						if (mIsEvent) {
							rp.put("colname", "data");
						} else {
							rp.put("colname", "data2");
						}
						*/
						rp.put("passwd", mPwd);
						rp.put("filename", file.getName());
						new TimedAsyncHttpResponseHandler(mAsyncHttpLooper, mContext).executePost(mTargetURI + "/gridfs", rp, bytes);
					} else {
						BufferedReader bufReader = new BufferedReader(new FileReader(file));
						String receiveString = null;
						StringBuilder stringBuilder = new StringBuilder();
						while ( (receiveString = bufReader.readLine()) != null ) {
							stringBuilder.append(receiveString);
						}
						bufReader.close();

						RequestParams rp = new RequestParams();
						rp.put("dbname", mDb);
						rp.put("colname", "event");
						/*
						if (mIsEvent) {
							rp.put("colname", "event");
						} else {
							rp.put("colname", "event2");
						}
						*/
						rp.put("passwd", mPwd);
						new TimedAsyncHttpResponseHandler(mAsyncHttpLooper, mContext).executePost(mTargetURI + "/col", rp, stringBuilder.toString());
					}
					Thread.sleep(1000); // limit the upload rate to avoid out of memory
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e){
					e.printStackTrace();
				}

				cnt += 1;
				publishProgress(cnt/fileList.size());
			}
		}

		return (long)1;
	}

	@Override
	protected void onProgressUpdate(Double... progress) {
		Log.i("upload", String.format("%.2f %%",progress[0]*100.0));
		Toast mToast = Toast.makeText(mContext, "upload: "+String.format("%.2f %%",progress[0]*100.0), Toast.LENGTH_SHORT);
		mToast.setGravity(Gravity.TOP, 0, 0);
		mToast.show();
	}

	protected void onPostExecute(Long result){
		Log.i("CommTask", "onPostExecute()");
		//trepn.stateUpdate(4, "CommTask.onPostExecute()");
	}

	private List<File> getListFiles(File parentDir) {
		ArrayList<File> inFiles = new ArrayList<File>();
		File[] files = parentDir.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				inFiles.addAll(getListFiles(file));
			} else {
				inFiles.add(file);
			}
		}
		return inFiles;
	}

	private  boolean isConnected(){
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null &&
				activeNetwork.isConnectedOrConnecting();
	}
}