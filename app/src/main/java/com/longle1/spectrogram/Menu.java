/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Menu extends Activity {
	Button button;
	EditText edittext1;
	EditText edittext2;
	EditText edittext3;
	CheckBox checkbox1;
	CheckBox checkbox2;
	CheckBox checkbox3;
	//CheckBox checkbox4;
	RadioGroup radioGroup1;
	RadioGroup radioGroup2;
	RadioButton radio11;
	RadioButton radio12;
	RadioButton radio21;
	RadioButton radio22;
	SeekBar seekBar1;
	//Spinner spinner1;
	//Spinner spinner;
	
	//BatteryLevelReceiver mBatteryLevelReceiver = new BatteryLevelReceiver();
	boolean start = false;

	final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
	String[] permissions= new String[]{
			Manifest.permission.WRITE_EXTERNAL_STORAGE,
			Manifest.permission.RECORD_AUDIO,
			Manifest.permission.ACCESS_FINE_LOCATION};

	private void checkAndRequestPermissions() {
		int result;
		List<String> listPermissionsNeeded = new ArrayList<>();
		for (String p:permissions) {
			result = ContextCompat.checkSelfPermission(this,p);
			if (result != PackageManager.PERMISSION_GRANTED) {
				listPermissionsNeeded.add(p);
			}
		}
		if (!listPermissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults) {
		switch (requestCode) {
			case REQUEST_ID_MULTIPLE_PERMISSIONS: {
				boolean isGranted = true;
				if (grantResults.length > 0){
					for (int i=0;i < grantResults.length;i++){
						if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
							isGranted = false;
						}
					}
				}

				if (isGranted){
					Log.i("Menu","all permissions granted");
				} else {
					Log.e("Menu","all permissions not granted");
					Toast mToast = Toast.makeText(this, "Insufficient permissions", Toast.LENGTH_LONG);
					TextView v = (TextView) mToast.getView().findViewById(android.R.id.message);
					v.setTextColor(Color.RED);
					mToast.show();
				}
				return;
			}

			// other 'case' lines to check for other
			// permissions this app might request
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		final Context mContext = this;
		button = (Button) findViewById(R.id.button1);
		edittext1 = (EditText) findViewById(R.id.editText1);
		edittext2 = (EditText) findViewById(R.id.editText2);
		edittext3 = (EditText) findViewById(R.id.editText3);
		checkbox1 = (CheckBox) findViewById(R.id.checkBox1);
		checkbox2 = (CheckBox) findViewById(R.id.checkBox2);
		checkbox3 = (CheckBox) findViewById(R.id.checkBox3);
		//checkbox4 = (CheckBox) findViewById(R.id.checkBox4);
		radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
		radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
		radio11 = (RadioButton) findViewById(R.id.radio11);
		radio12 = (RadioButton) findViewById(R.id.radio12);
		radio21 = (RadioButton) findViewById(R.id.radio21);
		radio22 = (RadioButton) findViewById(R.id.radio22);
		seekBar1 = (SeekBar) findViewById(R.id.seekBar1);
		//spinner1 = (Spinner) findViewById(R.id.spinner1);
		//spinner = (Spinner) findViewById(R.id.spinner);

		// ask for runtime permissions
		checkAndRequestPermissions();

		// The app root dir
		String sdcard = Environment.getExternalStorageDirectory().getPath();
        File dir = new File(sdcard, this.getString(this.getApplicationInfo().labelRes));
        if(!dir.exists()){
        	dir.mkdirs();
        }
		// Log app start time
        try {
        	File file = new File(dir, "appStartTime.txt");
        	OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
            outputStreamWriter.write(new Date().toString());
            outputStreamWriter.close();
		} catch (IOException  e) {
			e.printStackTrace();
		}

		edittext1.setText("http://acoustic.ifp.illinois.edu:8080");
		edittext2.setText("publicDb");
		edittext3.setText("publicPwd");
		try {
			File file = new File(dir, "config.txt");
			if (!file.exists()){
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
				outputStreamWriter.write("http://acoustic.ifp.illinois.edu:8080\npublicDb\npublicPwd");
				outputStreamWriter.close();
			}
			BufferedReader bufReader = new BufferedReader(new FileReader(file));
			edittext1.setText(bufReader.readLine());
			edittext2.setText(bufReader.readLine());
			edittext3.setText(bufReader.readLine());
			bufReader.close();
		} catch (IOException  e) {
			e.printStackTrace();
		}

        // Setup spinner
		/*
		List<String> list = new ArrayList<String>();
		list.add("all");
		list.add("MFCC");
		list.add("TFRidge");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
				android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
		*/
		/*
        List<String> list = new ArrayList<String>();
		list.add("Internet");
		list.add("Localnet");
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list){
		    public View getView(int position, View convertView, ViewGroup parent) {
		        View v = super.getView(position, convertView, parent);
		        ((TextView) v).setGravity(Gravity.CENTER);
		        ((TextView) v).setTextSize(25);
		        return v;
		    }

		    public View getDropDownView(int position, View convertView, ViewGroup parent) {
		        View v = super.getDropDownView(position, convertView,parent);
		        ((TextView) v).setGravity(Gravity.CENTER);
		        ((TextView) v).setTextSize(25);
		        return v;
		    }
		};
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner1.setAdapter(dataAdapter);
        
		spinner1.setOnItemSelectedListener(new OnItemSelectedListener (){
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				if (parent.getItemAtPosition(pos).toString().compareTo("Internet") == 0){
					edittext1.setText("http://acoustic.ifp.illinois.edu:8080\n"
						//+"publicDb\n"+"publicUser\n"+"publicPwd", BufferType.EDITABLE);
                        +"publicDb\n"+"publicPwd", BufferType.EDITABLE);
				}else{
					edittext1.setText("http://192.168.12.1:8080\n"
						//+"publicDb\n"+"publicUser\n"+"publicPwd", BufferType.EDITABLE);
                            +"publicDb\n"+"publicPwd", BufferType.EDITABLE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				edittext1.setText("http://acoustic.ifp.illinois.edu:8080\n"
					//+"publicDb\n"+"publicUser\n"+"publicPwd", BufferType.EDITABLE);
                    +"publicDb\n"+"publicPwd", BufferType.EDITABLE);
			}
		});
		*/
		// Setup "generalized" callbacks
		//this.registerReceiver(mBatteryLevelReceiver, new IntentFilter(Intent.ACTION_BATTERY_LOW));

		radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// checkedId is the RadioButton selected
				if (checkedId == radio11.getId()) {
					//((Spinner) spinner).getSelectedView().setEnabled(true);
					//spinner.setEnabled(true);
					seekBar1.setEnabled(true);
				} else {
					seekBar1.setEnabled(false);
				}

			}
		});

		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!start){
					// handle conflicting button/state
					if (GlobalState.getGlobalState().getBGstate() == true){
						start = true;
						button.setText(getResources().getString(R.string.stop)); // Change the button label
						return;
					}
					
					start = true;
					button.setText(getResources().getString(R.string.stop)); // Change the button label
					GlobalState.getGlobalState().setBGstate(true);
					GlobalState.getGlobalState().setGUIstate(checkbox1.isChecked());
					
			    	//String config = edittext1.getText().toString();
			    	//String[] splitted = config.split("\n");
			    	Bundle bundle = new Bundle();
				    bundle.putString("serverURI", edittext1.getText().toString());
                    bundle.putString("database", edittext2.getText().toString());
                    bundle.putString("password", edittext3.getText().toString());
				    //bundle.putString("devName", splitted[3]);
				    bundle.putString("androidID", Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID));
				    bundle.putInt("thresh", seekBar1.getProgress());
				    String radioButton1 = ((RadioButton)findViewById(radioGroup1.getCheckedRadioButtonId())).getText().toString();
				    String radioButton2 = ((RadioButton)findViewById(radioGroup2.getCheckedRadioButtonId())).getText().toString();
				    if ( radioButton2.equals(radio21.getText().toString()) ){
				    	bundle.putInt("datSrc", 0);// mic
					} else{
						bundle.putInt("datSrc", 1);// bluetooth
					}
					//bundle.putBoolean("isDebug", checkbox1.isChecked());
					bundle.putBoolean("isSendFeat", checkbox2.isChecked());
					//bundle.putInt("featType",spinner.getSelectedItemPosition());
					//bundle.putInt("featType",0);
					bundle.putBoolean("isSendAudio", checkbox3.isChecked());
				    if ( radioButton1.equals(radio11.getText().toString()) ){
				    	bundle.putBoolean("isEvent", true);
				    } else{
				    	bundle.putBoolean("isEvent", false);
				    }
				    
				    Intent processIntent;
				    processIntent = new Intent(mContext, ProcessService.class);
					processIntent.putExtra("Menu.Bundle", bundle);
			    	mContext.startService(processIntent);
			    	
			    	if ( GlobalState.getGlobalState().getGUIstate() ){
			    		Intent intent = new Intent(mContext, Spectrogram.class);
			    		startActivity(intent);
			    	}

					// store the current config in the config file
					String sdcard = Environment.getExternalStorageDirectory().getPath();
					File dir = new File(sdcard, mContext.getString(mContext.getApplicationInfo().labelRes));
					try {
						File file = new File(dir, "config.txt");
						OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
						outputStreamWriter.write(edittext1.getText()+"\n"+edittext2.getText()+"\n"+edittext3.getText());
						outputStreamWriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else{
					// handle conflicting button/state
					if (GlobalState.getGlobalState().getBGstate() == false){
						start = false;
						button.setText(getResources().getString(R.string.start));
						return;
					}
					
					start = false;
					button.setText(getResources().getString(R.string.start));
					GlobalState.getGlobalState().setBGstate(false);
					GlobalState.getGlobalState().setGUIstate(false);
				}                
			}
		});
	}
	
	@Override
	public void onDestroy() {
		GlobalState.getGlobalState().setBGstate(false);
    	super.onDestroy();
    }
	
	static {
    	System.loadLibrary("process");
		System.loadLibrary("dct");
    }
}
