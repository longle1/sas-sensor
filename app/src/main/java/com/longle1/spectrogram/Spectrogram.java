/*******************************************************************************
 * Copyright (c) 2014 University of Illinois at Urbana-Champaign.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Long Le, David Jun, and Douglas Jones - initial API and implementation
 *******************************************************************************/
package com.longle1.spectrogram;

import java.util.Collections;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

public class Spectrogram extends AppCompatActivity {

	private ImageView			imageView;
	private Bitmap				bitmap;
	private Canvas				canvas;
	private Paint				paint;
	private int		 			dimX = 256;
	private int					dimY = 192;
	
	private ResponseReceiver	receiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	public void onStart() {
    	super.onStart();
    	setContentView(R.layout.fragment_main);
    	imageView = (ImageView)this.findViewById(R.id.imageView1);
    	bitmap = Bitmap.createBitmap(dimX,dimY,Bitmap.Config.ARGB_8888);
    	canvas = new Canvas(bitmap);
		canvas.drawColor(Color.BLACK);
    	paint = new Paint();
    	imageView.setImageBitmap(bitmap);
    	
    	getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    	
    	 // Register a broadcast receiver
        receiver = new ResponseReceiver();
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, filter);
    }

	@Override
	public void onStop() {
    	getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    	unregisterReceiver(receiver);
    	GlobalState.getGlobalState().setGUIstate(false);
    	super.onStop();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
	public class ResponseReceiver extends BroadcastReceiver {
		public static final String ACTION_RESP =    
			      "edu.illinois.ifp.acoustic.intent.action.MESSAGE_PROCESSED";
		
		@Override
		public void onReceive(Context context, Intent intent) {
			GUIFrame guiFrame = (GUIFrame) intent.getExtras().getSerializable("GUIFrame");
	       
		   	int r,g,b;
			
		   	//Log.e("Recording GUIFrame", "Displaying...");
			
			// emulates a scrolling window
			Rect srcRect = new Rect(0, -(-1), bitmap.getWidth(), bitmap.getHeight());
			Rect destRect = new Rect(srcRect);
			destRect.offset(0, -1);
			canvas.drawBitmap(bitmap, srcRect, destRect, null);

			// draw spectrogram
			// update latest column with new values which must be between 0.0 and 1.0
			for(int i=0;i<guiFrame.spec.length;i++) {
				double val = guiFrame.spec[i];
				if (val < 0.0 || val > 1.0){
					Log.e("onProgressUpdate", String.format("%d, %f", guiFrame.spec.length, val));
					Log.e("onProgressUpdate", "Invalid double value");
					continue;
				}
			
				// simple linear RYGCB colormap
			    if(val <= 0.25) {
			        r = 0;
			        b = 255;
			        g = (int)(4*val*255);
			    } else if(val <= 0.5) {
			        r = 0;
			        g = 255;
			        b = (int)((1-4*(val-0.25))*255);
			    } else if(val <= 0.75) {
			        g = 255;
			        b = 0;
			        r = (int)(4*(val-0.5)*255);
			    } else {
			        r = 255;
			        b = 0;
			        g = (int)((1-4*(val-0.75))*255);
			    }
			
				// set color with constant alpha
				paint.setColor(Color.argb(255, r, g, b));
				// paint corresponding area
		    	canvas.drawPoint(i, dimY-1, paint);
			}
			
			// draw detection overlay
			/*
			int max_time = -1;
			if (guiFrame.TI.size() > 0){
				max_time = Collections.max(guiFrame.TI);
			}
			*/
			for(int k = 0; k < guiFrame.FI.size(); k++){
				//Log.i("onProgressUpdate", "Event detected");
				//paint.setColor(groupId2Color(guiFrame.groupId.get(k)));
				paint.setColor(Color.BLACK);
				paint.setAlpha(200); // 0 is transparent and 255 is opaque
		    	paint.setStyle(Paint.Style.STROKE);
		    	// draw a point
				int TIE = guiFrame.TI.get(guiFrame.TI.size()-1);
				// Estimate the delay between the GUI's rightmost point and
				// the ridge's last point is 10 frames
		    	canvas.drawPoint(guiFrame.FI.get(k), dimY-10-(TIE-guiFrame.TI.get(k)), paint);
		    	
		    	/*
	    	 	// draw path
		    	Path path = new Path();
		    	path.moveTo(guiFrame.FI.get(k).get(0), 
            		(dimY-1-(guiFrame.backtracklen+max_time-guiFrame.TI.get(k).get(0))));
		    	for (int l = 1; l < guiFrame.TI.get(k).size(); l++) {
		            path.lineTo(guiFrame.FI.get(k).get(l), 
	            		(dimY-1-(guiFrame.backtracklen+max_time-guiFrame.TI.get(k).get(l))));
		    	}
		    	canvas.drawPath(path, paint);
		    	*/
			}
			//newDisplayUpdate[0].rewind();
			imageView.invalidate();
		}
		
		@SuppressWarnings("unused")
		private int groupId2Color(int groupId){
			switch (groupId % 11){
				case 0:
					return Color.YELLOW;
				case 1:
					return Color.BLUE;
				case 2:
					return Color.CYAN;
				case 3:
					return Color.DKGRAY;
				case 4:
					return Color.GRAY;
				case 5:
					return Color.GREEN;
				case 6:
					return Color.LTGRAY;
				case 7:
					return Color.MAGENTA;
				case 8:
					return Color.RED;
				case 9:
					return Color.WHITE;
				case 10:
					return Color.BLACK;
				default:
					return Color.TRANSPARENT;
			}
		}
	}
}
