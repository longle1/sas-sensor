package com.longle1.spectrogram;

import java.util.Date;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

public class CommManager {
	final int HOLDTIME = 1; // wifi hold time, in minutes
	Context mContext;
	Date wifiStartDate;
	WifiManager wifiManager;
	ConnectivityManager connManager;
	
	public CommManager(Context context){
		mContext = context;

		connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
		wifiManager.setWifiEnabled(true);
		wifiStartDate = new Date();
	}

	public boolean isConnected(){
		NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();
		return activeNetwork != null &&
				activeNetwork.isConnectedOrConnecting();
		//return connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
		//return wifiManager.getWifiState()==WifiManager.WIFI_STATE_ENABLED;
	}

	public void wifiControl(boolean turnOn){
		if (turnOn) {
			wifiStartDate = new Date();
			if (!wifiManager.isWifiEnabled()){
				wifiManager.setWifiEnabled(true);
			}
		} else{
			if (wifiManager.isWifiEnabled()){
				if (wifiStartDate.before(new Date(System.currentTimeMillis()-HOLDTIME*60*1000))){
					wifiManager.setWifiEnabled(false);
				}
			}
		}
	}
}
