package com.longle1.spectrogram;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Long on 5/25/2016.
 */
public class Trepner {
    Context mContext;
    Trepner(Context iContext){
        mContext = iContext;
    }

    public void stateUpdate(int val, String msg){
        Intent stateUpdate = new Intent("com.quicinc.Trepn.UpdateAppState");
        stateUpdate.putExtra("com.quicinc.Trepn.UpdateAppState.Value",
                val);
        stateUpdate.putExtra("com.quicinc.Trepn.UpdateAppState.Value.Desc",
                msg);
        mContext.sendBroadcast(stateUpdate);
    }
}
