## Overview
The sensor component (highlighted in the figure below) in the overall SAS. The sensor is implemented as an Android app to take advantage of the pervasiveness of Android smartphones as an audio sensing platform. The app can be downloaded [here](https://play.google.com/store/apps/details?id=com.longle1.spectrogram).

![alt tag](https://bytebucket.org/longle1/sas-sensor/raw/7be97b4a927c042b464627518341347be25e37c3/fig2.png)

This is the repo of the swarm acoustic service ([SAS](http://acoustic.ifp.illinois.edu))'s sensor.

The repo for the server is at https://bitbucket.org/longle1/sas-servers

The repo for the client is at https://github.com/longle2718/sas-client

## Dependencies
This app uses the following [library](https://bitbucket.org/longle1/audio_class) for audio classification

## Acknowledgement
This work was supported in part by the TerraSwarm Research Center, one of six centers supported by the STARnet phase of the Focus Center Research Program (FCRP) a Semiconductor Research Corporation program sponsored by MARCO and DARPA. 

## License
The MIT License

Copyright (c) 2015 Long Le

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
